<?php include 'bootstrap.php';

class UsersController extends Controller {
    
    
    // Template;
    protected $template = 'users'; 
    
    protected $variables = array(
        'users' => '',
    ); 
    
    /**
    * Prerender function.
    */ 
    public function preRender() {
        // Fetching groceries
        // Query statement 
        $user = new User(); 
        $data = $user->fetchAll(); 

        if(!empty($data)) { 
            // Start Table tag 
            $table =  '<table>';  

            // Loop through each row in the database 
            // Store each row as an associative array 
            foreach ($data as $row) {
                // Opening row tag 
                $table .= '<tr>'; 

                // Output the name
                $table .= '<td>' . $row['username'] . '</td>';

                // Output Edit/Delete links with query strings. 
                $table .= '<td>';
                $table .= '<a href = "user.php?id=' . $row['id'] . '">Edit</a>'; 
                
                // Don't render delete link for current account. 
                if ($row['id'] != $_SESSION['user_id']) {
                    $table .= ' | <a href = "user-delete.php?id=' . $row['id'] . '">Delete</a>'; 
                }
                
                $table .= '</td>'; 
                // Closing row tag 
                $table .= '</tr>'; 

            }
            // Closing table tag 
            $table .= '</table>'; 
            
            $this->variables['users'] = $table; 
        }
    }
}
$session->isAuthorized();
$controller = new UsersController(); 
print $controller->run(); 