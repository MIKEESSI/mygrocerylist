<?php 
include 'bootstrap.php';

class IndexController extends Controller {
    protected $template = 'index';
}

$session->isAuthorized(); 
$controller = new IndexController(); 
print $controller->run(); 
