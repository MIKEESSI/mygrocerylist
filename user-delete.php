<?php 
include 'bootstrap.php';

class UserDeleteController extends Controller {
    
    protected $layoutTemplate = 'layout-no-menu'; 
    
    protected $template = 'user-delete'; 
    
    protected $variables = array(); 
    
    public function preRender() {
        if (!empty($_GET['id']) && $_GET['id'] != $_SESSION['user_id']) {
            $user = new User(); 
            $user->delete($_GET['id']); 
        }
    }
}

$session->isAuthorized(); 
$controller = new UserDeleteController();
print $controller->run(); 

