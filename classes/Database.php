<?php
class Database {
    
    private $db; 
    
    private static $instance; 
    
    public static function instance() {
        if (empty(self::$instance)) {
            self::$instance = new Database(DB_HOST, DB_USER, DB_PASS, DB_DATABASE); 
        }
        return self::$instance; 
    }
    
    public function __construct($host, $username, $password, $database) {
        $this->db = mysqli_connect($host, $username, $password, $database);  
    }
        
    private function parseArguments($sql, $variables) {
        $search = array(); 
        $replace = array(); 
        
        foreach ($variables as $key => $value) {
            $search[] = ':' . $key; 
            $replace[] = $value; 
        }
        
        return str_replace($search, $replace, $sql); 
    }
    
    public function select($sql, $variables = array()) {
        
        if (!empty($variables)) {
            $sql = $this->parseArguments($sql, $variables); 
        }
        
        $result = mysqli_query($this->db, $sql); 
        
        return mysqli_fetch_all($result, MYSQLI_ASSOC); 
    }
    
    public function query($sql, $variables = array()) {
        if (!empty($variables)) {
            $sql = $this->parseArguments($sql, $variables); 
        }

        return mysqli_query($this->db, $sql); 
    }
}