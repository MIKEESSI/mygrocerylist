<?php

class Item extends Model {
    
    public function insert($variables) {
        return $this->db->query("INSERT INTO `items` (`name`, `qty`, `user_id`) VALUES (':name', ':qty', ':user_id')", $variables); 
    }
    
    public function update($variables, $id) {
        return $this->db->query("UPDATE `items` SET 
            `name` = ':name',
            `qty` = ':qty'
            WHERE `id` = '" . $id . "'", $variables);
    }
    
    public function delete($id) {
        $this->db->query("DELETE FROM `items` WHERE `id` = '" . $id ."'"); 
    }
    
    public function fetchAll() {
        return $this->db->select("SELECT * FROM `items`"); 
    }
    
    public function fetch($item_id) {
        $items = $this->db->select("SELECT * FROM `items` WHERE `id` = '" . $item_id ."'"); 
        return $items[0]; 
    }
    
    public function fetchUserItems($user_id) {
        return $this->db->select("SELECT * FROM `items` WHERE `user_id` = '" . $user_id . "'"); 
    } 
}