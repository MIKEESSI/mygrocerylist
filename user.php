<?php 
include 'bootstrap.php'; 

class UserController extends Controller {

    protected $template = 'user'; 

    protected $variables = array(
        'error_message' => '',
        'username' => '',
        'password' => '', 
    ); 

    public function preRender() {

        // Item object. 
        $user = new User(); 
        
        // If query string is set 
        if (!empty($_GET['id'])) {

            // Select that row from the database 
            $data = $user->fetch($_GET['id']); 
            $this->variables['username'] = $data['username']; 
        }
        
        // Form submit handler 
        if (!empty($_POST)) {

            foreach($_POST as $key => $value) {
                // Sanitize input
                $value = strip_tags($value); 

                // Save sanitized post values into form array 
                // This array is used to populate the form after form submission. 
                $_POST[$key] = $value; 
            }

            if (!empty($_POST['username']) && !empty($_POST['password'])) {

                // If query string is set then update record 
                if (!empty($_GET['id'])) {
            
                    $fields = array(
                        'username' => $_POST['username'],
                        'password' => $_POST['password']
                    ); 
                    $user->update($fields, $_GET['id']); 

                    header('location:users.php'); 

                // If ID is not set then insert record
                } else {
                    
                    // Insert new item. 
                   $fields = array(
                        'username' => $_POST['username'],
                        'password' => $_POST['password'],
                    ); 
                    $user->insert($fields); 
                    
                    // Run query.
                    header('location:users.php'); 
                }
            } else {
                $this->variables['error_message'] = "Please provide some values.";
            }
        }
    }
}

$session->isAuthorized(); 
$controller = new UserController(); 
print $controller->run(); 
